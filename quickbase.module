<?php
/**
 * @file
 * 
 * This file contains the main module codebase
 */

define('QB_CFG_USERNAME', 'username');
define('QB_CFG_PASSWORD', 'password');
define('QB_CFG_REALM', 'realm');
define('QB_CFG_TOKEN', 'apptoken');

/**
 * Implements hook_menu
 */
function quickbase_menu() {
  $items = array();

  // Main configuration settings page
  $items['admin/config/system/quickbase'] = array(
    'title' => 'QuickBase API',
    'description' => "Configure the QuickBase API module to integrate against Intuit's QuickBase service.",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('quickbase_settings_form'),
    'type' => MENU_NORMAL_ITEM, 
    'access arguments' => array('administer quickbase'),
    'file' => 'quickbase.admin.inc',
  );

  $items['quickbase/test'] = array(
    'title' => 'QuickBase API Test Page',
    'page callback' => '_quickbase_test',
    'type' => MENU_NORMAL_ITEM, 
    'access arguments' => array('administer quickbase'),
    'file' => 'quickbase.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_permission
 */
function quickbase_permission() {
  $perms = array();

  $perms['administer quickbase'] = array(
    'title' => t('Administer QuickBase API settings'),
    'description' => t("Allows users to configure the QuickBase API module's system settings"),
    'restrict access' => TRUE,
  );

  return $perms;
}

/**
 * Implements hook_libraries_info
 */
function quickbase_libraries_info() {
  $libs = array();

  $libs['quickbase-api'] = array(
    'name' => 'Intuit QuickBase API - PHP Class Library',
    'vendor url' => 'http://quickbase-api.octobang.com/',
    'download url' => 'http://git.octobang.com/quickbase-api',
    'version arguments' => array(
      'file' => 'quickbase-api.php',
      'pattern' => '/(quickbase)/',
    ),
    'files' => array(
      'php' => array('quickbase-api.php'),
    ),
  );

  return $libs;
}

/**
 * Public function for creating and initializing the QuickBase SDK object
 * 
 * @param $params
 *   Array of key-value pairs overriding the default authentication parameters set via the admin UI:
 *     QB_CFG_USERNAME:    username
 *     QB_CFG_PASSWORD:    password
 *     QB_CFG_REALM:       authentication realm/domain
 *     QB_CFG_TOKEN:       application security token
 * 
 * @return
 *   Returns an initialized QuickBase SDK object, or NULL on failure
 */
function quickbase($params = array()) {
  $obj = NULL;

  // Merge admin settings with passed in params to allow for flexibilty
  $cfg = array_merge(_quickbase_settings(), $params);

  $library = _quickbase_load_lib();
  if (!empty($library['loaded'])) {
    if (!empty($cfg[QB_CFG_USERNAME]) && !empty($cfg[QB_CFG_PASSWORD]) && !empty($cfg[QB_CFG_REALM])) {
      $obj = new QuickBaseAPI($cfg);
    }
    else {
      watchdog('quickbase', 'QuickBase API not configured properly', NULL, WATCHDOG_ERROR, l('QuickBase API settings', 'admin/config/system/quickbase'));
    }
  }
  else {
    watchdog('quickbase', 'Unable to load QuickBase library (QuickBase PHP SDK)', NULL, WATCHDOG_ERROR, l('QuickBase API settings', 'admin/config/system/quickbase'));
  }

  return $obj;
}

/**
 * Util function for loading the external PHP class library
 */
function _quickbase_load_lib() {
  $library = libraries_load('quickbase-api');
  return $library;
}

/**
 * Util function for saving/retrieving QuickBase API settings
 */
function _quickbase_settings($cfg = NULL) {
  if ($cfg) {
    variable_set('quickbase_settings', $cfg);
  }
  else {
    return variable_get('quickbase_settings', array());
  }
}
