<?php
/**
 * @file
 * 
 * This file contains the normal page callbacks for the module
 */

/**
 * Util function for testing QuickBase API calls
 */
function _quickbase_test() {
  $output = '';
  $qb = quickbase();
  if ($qb) {
    $output .= '<pre>' . print_r($qb, TRUE) . '</pre>';
    $resp = $qb->GrantedDBs();
    $output .= '<pre>' . print_r($resp, TRUE) . '</pre>';
    //$output .= print_r($resp, TRUE);
  }
  return $output;
}
